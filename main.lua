local FaithTarots = RegisterMod("Tarots of Faith", 1)
local game = Game()
local itemPool = game:GetItemPool()
local itemConfig = Isaac.GetItemConfig()
local ModRNG = RNG()

FaithTarots.WornCard = Isaac.GetCardIdByName("Worn Card")
FaithTarots.RunicDesires = Isaac.GetItemIdByName("Runic Desires")
FaithTarots.GrowthCrystal = Isaac.GetItemIdByName("Growth Crystal")
FaithTarots.SackofCards = Isaac.GetItemIdByName("Sack of Cards")
FaithTarots.SackofCardsEnt = Isaac.GetEntityVariantByName("Sack of Cards")

if not __eidCardDescriptions then __eidCardDescriptions = {} end
__eidCardDescriptions[FaithTarots.WornCard] = "Triggers a random card."

if not __eidItemDescriptions then __eidItemDescriptions = {} end
__eidItemDescriptions[FaithTarots.RunicDesires] = "Cards and pills have a 15% chance of getting replaced by a rune."
__eidItemDescriptions[FaithTarots.GrowthCrystal] = "Spawns a random rune."
__eidItemDescriptions[FaithTarots.SackofCards] = "Spawns random cards every 6 rooms."

-- Register mod functions.
function FaithTarots.GetNumCards()
	local CardTable = {}
	
	for i = 0, 9999 do
		local card = itemConfig:GetCard(i)
		
		if card then
			if card.ID < Card.RUNE_HAGALAZ or card.ID > Card.RUNE_BLACK then
				CardTable[#CardTable + 1] = card.ID
			end
		else
			break
		end
	end
	
	return CardTable
end

function mod.GetPlayerUsingItem(itemID) -- To get the player using an item.
    for p = 0, game:GetNumPlayers() - 1 do
        local player = Isaac.GetPlayer(p)
        
        if (player:GetActiveItem() == itemID or player:GetActiveItem() == CollectibleType.COLLECTIBLE_VOID)
		and (Input.IsActionTriggered(ButtonAction.ACTION_ITEM, player.ControllerIndex) or Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, player.ControllerIndex)) then
            return player
        end
    end

    return Isaac.GetPlayer(0)
end

-- Seed the RNG.
FaithTarots:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function()
	ModRNG:SetSeed(game:GetSeeds():GetStartSeed(), 35)
end)

---------==--------
-- Sack of Cards --
---------==--------

-- To spawn the familiar
FaithTarots:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player, cacheFlag)
	if player:HasCollectible(FaithTarots.SackofCards) then
		local famRNG = RNG()
		famRNG:SetSeed(player.InitSeed, 35)
		
		player:CheckFamiliar(FaithTarots.SackofCardsEnt, player:GetCollectibleNum(FaithTarots.SackofCards), famRNG)
	end
end, CacheFlag.CACHE_FAMILIARS)

-- To update the famiilar.
FaithTarots:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, fam)
	local sprite = fam:GetSprite()
	local data = fam:GetData()
	if not data.RoomCount then data.RoomCount = 0 end
	
	if not data.SackofCardsInit then
		fam:AddToFollowers()
		data.SackofCardsInit = true
	end
	
	if not data.PlayIdle then
		sprite:Play("FloatDown", false)
	end
	
	if sprite:IsFinished("Spawn") then
		data.PlayIdle = nil
		
		-- Proc Ancient recall.
		fam.Player:UseCard(Card.CARD_ANCIENT_RECALL)
		fam.Player:PlayExtraAnimation("HideItem")
		fam.Player:GetSprite().PlaybackSpeed = 8
		fam.Player:Update()
		fam.Player:GetSprite().PlaybackSpeed = 1
		
		-- Reposition the cards to the sack of card's pos.
		local CardCount = 3
		for _, ent in pairs(Isaac.FindByType(5, PickupVariant.PICKUP_TAROTCARD, -1)) do
			if CardCount > 0 then
				if not ent:GetData().SackofCardsSpawn then
					CardCount = CardCount - 1
					ent.Position = fam.Position
					ent:GetData().SackofCardsSpawn = true
				end
			else
				break
			end
		end
	end
	
	local CountModifier = fam.Player:HasCollectible(CollectibleType.COLLECTIBLE_BFFS) and 4 or 6
	if data.RoomCount >= CountModifier then
		data.RoomCount = data.RoomCount - CountModifier
		data.PlayIdle = true
		sprite:Play("Spawn", true)
	end
	
	fam:FollowParent()
end, FaithTarots.SackofCardsEnt)

-- To increase the room count.
FaithTarots:AddCallback(ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD, function(_, rng, pos)
	for _, ent in pairs(Isaac.FindByType(EntityType.ENTITY_FAMILIAR, FaithTarots.SackofCardsEnt, -1)) do
		ent:GetData().RoomCount = ent:GetData().RoomCount + 1
	end
end)

---------==--------
-- Grwth Crystal --
---------==--------

local RuneChanceTable = {
	Card.RUNE_BERKANO,	Card.RUNE_BERKANO,	Card.RUNE_BERKANO,	Card.RUNE_BERKANO,
	Card.RUNE_DAGAZ,	Card.RUNE_DAGAZ,	Card.RUNE_DAGAZ,
	Card.RUNE_HAGALAZ,	Card.RUNE_HAGALAZ,	Card.RUNE_HAGALAZ,
	Card.RUNE_EHWAZ,	Card.RUNE_EHWAZ,
	Card.RUNE_ALGIZ,	Card.RUNE_ALGIZ,
	Card.RUNE_ANSUZ,	Card.RUNE_ANSUZ,
	Card.RUNE_JERA,
	Card.RUNE_PERTHRO,
	Card.RUNE_BLANK,
	Card.RUNE_BLACK,
}

-- To spawn the rune when using the itme.
FaithTarots:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, itemID, rng)
	local player = FaithTarots.GetPlayerUsingItem(itemID)
	local RuneRoll = RuneChanceTable[rng:RandomInt(#RuneChanceTable) + 1]
	
	Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, RuneRoll, game:GetRoom():FindFreePickupSpawnPosition(player.Position, 0, true), Vector(0, 0), player)
	
	return true
end, FaithTarots.GrowthCrystal)

-- To make batteries only give 6 charges to the item.
FaithTarots:AddCallback(ModCallbacks.MC_POST_PLAYER_RENDER, function(_, player) -- using render to update the hud in real time.
	if player:GetActiveItem() == FaithTarots.GrowthCrystal then
		local data = player:GetData()
		local currentCharges = player:GetActiveCharge()
		if not data.GrowthCrystalCharges then data.GrowthCrystalCharges = player:GetActiveCharge() end
		
		if data.GrowthCrystalCharges + 6 < currentCharges then
			player:SetActiveCharge(data.GrowthCrystalCharges + 6)
		end
		
		data.GrowthCrystalCharges = player:GetActiveCharge()
	end
end)

---------==--------
-- Runic desires --
---------==--------

-- To replace cards and pills.
FaithTarots:AddCallback(ModCallbacks.MC_POST_PICKUP_SELECTION, function(_, pickup, variant, subtype)
	if game:GetRoom():GetFrameCount() == 0 then return end
	
	local RerollChance = 0.15
	local RunChance = false
	for p = 0, game:GetNumPlayers() - 1 do
		local player = Isaac.GetPlayer(p)
		
		if player:HasCollectible(FaithTarots.RunicDesires) then
			RerollChance = math.min(RerollChance + (player:GetCollectibleNum(FaithTarots.RunicDesires) * 0.05), 0.4)
			RunChance = true
		end
	end
	
	if RunChance and ModRNG:RandomFloat() < RerollChance then
		if variant == PickupVariant.PICKUP_TAROTCARD or variant == PickupVariant.PICKUP_PILL then
			local RuneRoll = ModRNG:RandomInt(9) + 32
			return {PickupVariant.PICKUP_TAROTCARD, RuneRoll}
		end
	end
end)

---------==--------
--   Worn Card   --
---------==--------

-- To replace the card graphic.
FaithTarots:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
	if pickup.SubType == FaithTarots.WornCard then
		local sprite = pickup:GetSprite()
		sprite:ReplaceSpritesheet(0, "gfx/items/worn_card_spr.png")
		sprite:LoadGraphics()
		Isaac.DebugString("bup")
	end
end, PickupVariant.PICKUP_TAROTCARD)

-- To use the worn card and proc a random effect.
FaithTarots:AddCallback(ModCallbacks.MC_USE_CARD, function(_, cardID)
	for p = 0, game:GetNumPlayers() - 1 do
		local player = Isaac.GetPlayer(p)
		
		if (Input.IsActionTriggered(ButtonAction.ACTION_ITEM, player.ControllerIndex)
		or Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, player.ControllerIndex)) then
			local CardTable = FaithTarots.GetNumCards()
			local CardRoll = ModRNG:RandomInt(#CardTable) + 1
			
			player:UseCard(CardTable[CardRoll])
		end
	end
end, FaithTarots.WornCard)

-- To spawn a worn card.
FaithTarots:AddCallback(ModCallbacks.MC_GET_CARD, function(_, rng, cardID, pCard, runes, oRunes)
	if cardID < 32 or cardID > 41 then
		local roll = rng:RandomFloat()
		local TotCards = FaithTarots.GetNumCards()
		local Chance = 1 / #TotCards
		
		if roll <= Chance then
			return FaithTarots.WornCard
		end
	end
end)


--------------------------------==--------------------------------
--------------------------------==--------------------------------
Isaac.DebugString("Tarots of Faith: Loaded Successfully!")
--------------------------------==--------------------------------
--------------------------------==--------------------------------
